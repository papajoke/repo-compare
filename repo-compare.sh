#!/bin/bash
set -euo pipefail
#set -x

# Get pacman and vercmp
if [[ ! -f pacman-static ]]; then
	curl "https://pkgbuild.com/~eschwartz/repo/x86_64/pacman-static-5.1.3-6-x86_64.pkg.tar.xz" | \
		bsdtar --strip-components=2 -xf - \*pacman-static \*vercmp-static
	chmod u+x pacman-static vercmp-static
fi

# Refresh package lists
declare -r mirror="https://mirror.netzspielplatz.de/manjaro/packages"

for branch in stable testing unstable; do
	mkdir -p $branch/sync
	for repo in core extra community multilib; do
		wget -qN $mirror/$branch/$repo/x86_64/$repo.db -P $branch/sync/
	done
done

# Process each repo in turn
for repo in core extra community multilib; do
	mapfile -t map < <(join -a1 -a2 -e "n/a" -o auto --nocheck-order <(join -a1 -a2 -e "n/a" -o auto --nocheck-order \
		<(./pacman-static --config 'pacman.conf' -Sl -b stable "$repo" | sed "s|$repo ||" | sort) \
		<(./pacman-static --config 'pacman.conf' -Sl -b testing "$repo" | sed "s|$repo ||" | sort)) \
		<(./pacman-static --config 'pacman.conf' -Sl -b unstable "$repo" | sed "s|$repo ||" | sort))

	cat > "$repo".html << EOH
<!DOCTYPE html>
<html>
<head>
<title>Repository comparison for $repo</title>
<link rel="stylesheet" type="text/css" href="assets/datatables.min.css">
</head>
<body>
<p><a href="core.html">core</a> | <a href="extra.html">extra</a> | <a href="community.html">community</a> | <a href="multilib.html">multilib</a></p>
<h1>$repo</h1>
<p>Last updated: $(date)</p>
<table id="main" class="display compact">
<thead><tr><th>Package</th><th>stable</th><th>testing</th><th>unstable</th></tr></thead>
EOH

	for package in "${map[@]}"; do
		tstyle=''
		ustyle=''
		IFS=$' ' read -r name stable testing unstable <<< "$package"
		[[ $(./vercmp-static "$testing"  "$stable")  -eq 1 ]] && tstyle=' style="color:#9f0000"' && ustyle="$tstyle"
		[[ $(./vercmp-static "$unstable" "$testing") -eq 1 ]] && ustyle=' style="color:#048000"'
		echo "<tr><td>$name</td><td>$stable</td><td$tstyle>$testing</td><td$ustyle>$unstable</td></tr>" >> "$repo".html
	done

	cat >> "$repo".html << "EOF"
<script type="text/javascript" charset="utf8" src="assets/datatables.min.js"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#main').DataTable( {
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
			ordering: false
		} );
	} );
</script>
</body>
</html>
EOF
done
